#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"

// Connect stepper motors with 200 steps per revolution (1.8 degree)
Adafruit_MotorShield AFMS1 = Adafruit_MotorShield();
Adafruit_MotorShield AFMS2 = Adafruit_MotorShield(0x61);
Adafruit_StepperMotor *motor_x = AFMS1.getStepper(200, 1);
Adafruit_StepperMotor *motor_y = AFMS1.getStepper(200, 2);
Adafruit_StepperMotor *motor_z = AFMS2.getStepper(200, 1);

// Speed in rpm
int default_speed = 50;
int motor_x_speed = 100;
int motor_y_speed = 100;
int motor_z_speed = 100;

// TODO: check if this number is right; distance per thread (8mm) * 1.8/360
double distance_per_step = 0.00157480315; // in inches
double paper_size = 8.5; // paper dimensions in inches (assuming square sheet)
double cur_x;
double cur_y;

void moveToOrigin() {
  // move to origin; use limit switches to check
  cur_x = 0;
  cur_y = 0;
}

void moveTo(double crease_pattern_size, double x, double y) {
  x = x * (paper_size / crease_pattern_size);
  y = y * (paper_size / crease_pattern_size);

  int direction_x;
  if (x >= cur_x) direction_x = FORWARD;
  else            direction_x = BACKWARD;

  int direction_y;
  if (y >= cur_y) direction_y = FORWARD;
  else            direction_y = BACKWARD;

  // TODO: make sure these don't exceed max speed
  motor_x->setSpeed(default_speed);
  motor_y->setSpeed(abs((y - cur_y) / (x - cur_x)) * default_speed);

  // TODO: figure out how to deal with rounding error
  int num_steps_x = round(abs(x - cur_x) / distance_per_step);
  int num_steps_y = round(abs(y - cur_y) / distance_per_step);

  // TODO: figure out why these two lines make everything stop
  motor_x->step(num_steps_x, direction_x, SINGLE);
  motor_y->step(num_steps_y, direction_y, SINGLE);

  cur_x = x;
  cur_y = y;
}

void raiseStylus() {
  Serial.println("raising stylus");
}

void lowerStylus() {
  Serial.println("lowering stylus");
}

void flipPaper() {
  Serial.println("*** flipping paper ***");
}

void makeCrease(double crease_pattern_size, double x1, double y1, double x2, double y2) {
  Serial.print("Making crease from ("); Serial.print(x1); Serial.print(", "); Serial.print(y1);
  Serial.print(") to ("); Serial.print(x2); Serial.print(", "); Serial.print(y2); Serial.print(")");
  Serial.println();

  raiseStylus();
  moveTo(crease_pattern_size, x1, y1);
  lowerStylus();
  moveTo(crease_pattern_size, x2, y2);
}

void makeCreasePattern(double crease_pattern_size, double valley_creases[][4], double mountain_creases[][4]) {
  Serial.println("*** ");
  for (int i = 0; i < sizeof(valley_creases); i++) {
    makeCrease(crease_pattern_size, valley_creases[i][0], valley_creases[i][1], valley_creases[i][2], valley_creases[i][3]);
  }

  flipPaper();

  for (int i = 0; i < sizeof(mountain_creases); i++) {
    makeCrease(crease_pattern_size, mountain_creases[i][0], mountain_creases[i][1], mountain_creases[i][2], mountain_creases[i][3]);
  }
  Serial.println("*** ");
}

void setup() {
  Serial.begin(9600);
  Serial.println("Begin!");

  // create with the default frequency 1.6KHz
  AFMS1.begin();
  AFMS2.begin();

  motor_x->setSpeed(motor_x_speed);
  motor_y->setSpeed(motor_y_speed);
  motor_z->setSpeed(motor_z_speed);

  moveToOrigin();

  // Hard-coded for now; TODO: do file io or something
  double crease_pattern_size = 10;
  double valley_creases[][4]   = {{0, 0, 10, 10},
                                  {10, 0, 0, 10}};
  double mountain_creases[][4] = {{5, 0, 5, 10},
                                  {0, 5, 10, 5}};
  makeCreasePattern(crease_pattern_size, valley_creases, mountain_creases);
}

void loop() {
}